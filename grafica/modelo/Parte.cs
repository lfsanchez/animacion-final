﻿using OpenTK.Graphics.OpenGL4;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;
using grafica.Properties;
using System.IO;

namespace grafica
{
    public class Parte
    {
        float[] vertices;
        int VertexBufferObject;
        int VertexArrayObject;
        public Shader shader;
        public Texturas textura;
        public Vector3 pos;
        public Vector3 tras;
        public Vector3 angulo;
        public Vector3 sca;
        float ancho;
        float largo;
        float alto;

        public Parte(Vector3 posicion, float Ancho, float Largo, float Alto, Vector3 t, Vector3 angle, Vector3 s, Vector2 X, Vector2 Y)
        {
            this.pos = posicion;
            this.ancho = Ancho;
            this.largo = Largo;
            this.alto = Alto;
            this.tras = t;
            this.angulo = angle;
            this.sca = s;
            vertexs(pos, ancho, largo, alto, X, Y);
        }

        public void Load()
        {
            shader = new Shader();
            byte[] text;
            using (MemoryStream ms = new MemoryStream())
            {
                Resources.texturas.Save(ms, Resources.texturas.RawFormat);
                text = ms.ToArray();
            }
            textura = new Texturas(text);
            VertexArrayObject = GL.GenVertexArray();
            GL.BindVertexArray(VertexArrayObject);
            VertexBufferObject = GL.GenBuffer();
            GL.BindBuffer(BufferTarget.ArrayBuffer, VertexBufferObject);
            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, true, 5 * sizeof(float), 0);
            int texCoordLocation = shader.GetAttribLocation("aTexCoord");
            GL.EnableVertexAttribArray(texCoordLocation);
            GL.VertexAttribPointer(texCoordLocation, 2, VertexAttribPointerType.Float, true, 5 * sizeof(float), 3 * sizeof(float));
            GL.EnableVertexAttribArray(0);
            shader.Use();
        }

        void vertexs(Vector3 p, float an, float lar, float alt, Vector2 X, Vector2 Y)
        {
            this.vertices = new float[]
            {
                //base superior
                p.X,     p.Y,     p.Z-an, X.X, Y.Y,
                p.X,     p.Y,     p.Z,    X.X, Y.X,
                p.X+lar, p.Y,     p.Z,    X.Y, Y.X,

                p.X+lar, p.Y,     p.Z,    X.Y, Y.X,
                p.X,     p.Y,     p.Z-an, X.X, Y.Y,
                p.X+lar, p.Y,     p.Z-an, X.Y, Y.Y,
                //base inferior
                p.X,     p.Y-alt, p.Z-an, X.X, Y.Y,
                p.X,     p.Y-alt, p.Z,    X.X, Y.X,
                p.X+lar, p.Y-alt, p.Z,    X.Y, Y.X,

                p.X+lar, p.Y-alt, p.Z,    X.Y, Y.X,
                p.X,     p.Y-alt, p.Z-an, X.X, Y.Y,
                p.X+lar, p.Y-alt, p.Z-an, X.Y, Y.Y,
                //borde frontal
                p.X,     p.Y,     p.Z,    X.X, Y.Y,
                p.X,     p.Y-alt, p.Z,    X.X, Y.X,
                p.X+lar, p.Y,     p.Z,    X.Y, Y.Y,

                p.X,     p.Y-alt, p.Z,    X.X, Y.X,
                p.X+lar, p.Y,     p.Z,    X.Y, Y.Y,
                p.X+lar, p.Y-alt, p.Z,    X.Y, Y.X,
                //borde trasero
                p.X,     p.Y,     p.Z-an, X.X, Y.Y,
                p.X,     p.Y-alt, p.Z-an, X.X, Y.X,
                p.X+lar, p.Y,     p.Z-an, X.Y, Y.Y,

                p.X,     p.Y-alt, p.Z-an, X.X, Y.X,
                p.X+lar, p.Y,     p.Z-an, X.Y, Y.Y,
                p.X+lar, p.Y-alt, p.Z-an, X.Y, Y.X,
                //borde lateral izquierdo
                p.X,     p.Y,     p.Z,    X.X, Y.Y,
                p.X,     p.Y-alt, p.Z,    X.X, Y.X,
                p.X,     p.Y,     p.Z-an, X.Y, Y.Y,

                p.X,     p.Y,     p.Z-an, X.Y, Y.Y,
                p.X,     p.Y-alt, p.Z,    X.X, Y.X,
                p.X,     p.Y-alt, p.Z-an, X.Y, Y.X,
                //borde lateral derecho
                p.X+lar, p.Y,     p.Z,    X.X, Y.Y,
                p.X+lar, p.Y-alt, p.Z,    X.X, Y.X,
                p.X+lar, p.Y,     p.Z-an, X.Y, Y.Y,

                p.X+lar, p.Y,     p.Z-an, X.Y, Y.Y,
                p.X+lar, p.Y-alt, p.Z,    X.X, Y.X,
                p.X+lar, p.Y-alt, p.Z-an, X.Y, Y.X,
            };
            Load();
        }
        public void unload()
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.DeleteBuffer(VertexBufferObject);
            shader.Dispose();
        }

        public void render(float width, float height)
        {
            Matrix4 view = Matrix4.CreateTranslation(tras.X, tras.Y, tras.Z);
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), width / height, 0.1f, 100.0f);
            Matrix4 X;
            Matrix4 Y;
            Matrix4 Z;
            X = Matrix4.CreateRotationX(angulo.X);
            Y = Matrix4.CreateRotationY(angulo.Y);
            Z = Matrix4.CreateRotationZ(angulo.Z);
            Matrix4 model = X * Y * Z;
            Matrix4 scale = Matrix4.CreateScale(sca);
            shader.SetMatrix4("model", model);
            shader.SetMatrix4("view", view);
            shader.SetMatrix4("projection", projection);
            shader.SetMatrix4("scale", scale);
            GL.BindVertexArray(VertexArrayObject);
            GL.DrawArrays(PrimitiveType.Triangles, 0, vertices.Length);
        }
    }
}

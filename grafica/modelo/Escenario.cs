﻿using OpenTK;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace grafica
{
    class Escenario
    {
        public Dictionary<string, Dibujo> dibujos;
        private int actual = 1;

        public Escenario()
        {
            dibujos = new Dictionary<string, Dibujo>();
        }
        public void addDibujo(String nombre,Dibujo act)
        {
            dibujos.Add(nombre, act);
        }
        public void Load()
        {
            
        }

        public void Unload()
        {
            foreach(KeyValuePair<string, Dibujo> act in dibujos)
            {
                act.Value.unload();
            }
        }
        public void changeDibujo(String key, Object tras, Object rot, Object sca)
        {
            dibujos.GetValueOrDefault(key).changeall(tras, rot, sca);
        }

        public void changeParte(String key, String parte, Object tras, Object rot, Object sca)
        {
            dibujos.GetValueOrDefault(key).changeItem(parte, tras, rot, sca);
        }

        public void changeall(Object tras, Object rot, Object sca)
        {
            foreach (KeyValuePair<string, Dibujo> aux in dibujos)
            {
                aux.Value.changeall(tras, rot, sca);
            }
        }

        public void render(Vector2 Size)
        {
            foreach (KeyValuePair<string, Dibujo> act in dibujos)
            {
                act.Value.render(Size.X, Size.Y);
            }
        }

        public void changeActual()
        {
            if(this.actual >= dibujos.Count)
            {
                this.actual = 1;
            }
            else
            {
                this.actual = this.actual + 1;
            }
        }
        public string Dactual()
        {
            int i = 1;
            foreach (KeyValuePair<string, Dibujo> act in dibujos)
            {
                if(i == this.actual)
                {
                    return act.Key;
                }
                i++;
            }
            return null;
        }
    }
}

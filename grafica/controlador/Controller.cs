﻿using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace grafica.controlador
{
    class Controller
    {
        bool escena;
        bool dibujo;
        Timer time;

        public Controller()
        {
            escena = true;
            dibujo = false;
            time = new Timer();
        }

        public Escenario Tecla(Escenario esce, String tecla)
        {
            if(tecla == "D1")
            {
                escena = true;
                dibujo = false;
            }
            if (tecla == "D2")
            {
                escena = false;
                dibujo = true;
            }
            if (tecla == "D3")
            {
                escena = false;
                dibujo = false;
            }
            if (tecla == "W")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(0.01f, 0, 0), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(0.01f, 0, 0), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(0.01f, 0, 0), null);
                    }
                }
            }
            if (tecla == "S")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(-0.01f, 0, 0), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(-0.01f, 0, 0), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(-0.01f, 0, 0), null);
                    }
                }
            }
            if (tecla == "A")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(0, 0.01f, 0), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(0, 0.01f, 0), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(0, 0.01f, 0), null);
                    }
                }
            }
            if (tecla == "D")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(0, -0.01f, 0), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(0, -0.01f, 0), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(0, -0.01f, 0), null);
                    }
                }
            }
            if (tecla == "Q")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(0, 0, 0.01f), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(0, 0, 0.01f), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(0, 0, 0.01f), null);
                    }
                }
            }
            if (tecla == "E")
            {
                if (escena)
                {
                    esce.changeall(null, new Vector3(0, 0, -0.01f), null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, new Vector3(0, 0, -0.01f), null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, new Vector3(0, 0, -0.01f), null);
                    }
                }
            }
            if (tecla == "G")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(0, 0, 0.01f), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0, 0, 0.01f), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0, 0, 0.01f), null, null);
                    }
                }
            }
            if (tecla == "T")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(0, 0, -0.01f), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0, 0, -0.01f), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0, 0, -0.01f), null, null);
                    }
                }
            }
            if (tecla == "F")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(-0.01f, 0, 0), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(-0.01f, 0, 0), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(-0.01f, 0, 0), null, null);
                    }
                }
            }
            if (tecla == "H")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(0.01f, 0, 0), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0.01f, 0, 0), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0.01f, 0, 0), null, null);
                    }
                }
            }
            if (tecla == "X")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(0, 0.01f, 0), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0, 0.01f, 0), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0, 0.01f, 0), null, null);
                    }
                }
            }
            if (tecla == "Space")
            {
                if (escena)
                {
                    esce.changeall(new Vector3(0, -0.01f, 0), null, null);
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0, -0.01f, 0), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0, -0.01f, 0), null, null);
                    }
                }
            }
            if (tecla == "I")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(0, 0, -0.01f));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), new Vector3(0, 0, -0.01f), null, null);
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), new Vector3(0, 0, -0.01f), null, null);
                    }
                }
            }
            if (tecla == "K")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(0, 0, 0.01f));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, null, new Vector3(0, 0, 0.01f));
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, null, new Vector3(0, 0, 0.01f));
                    }
                }
            }
            if (tecla == "J")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(0.01f, 0, 0));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, null, new Vector3(0.01f, 0, 0));
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, null, new Vector3(0.01f, 0, 0));
                    }
                }
            }
            if (tecla == "L")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(-0.01f, 0, 0));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, null, new Vector3(-0.01f, 0, 0));
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, null, new Vector3(-0.01f, 0, 0));
                    }
                }
            }
            if (tecla == "U")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(0, 0.01f, 0));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, null, new Vector3(0, 0.01f, 0));
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, null, new Vector3(0, 0.01f, 0));
                    }
                }
            }
            if (tecla == "O")
            {
                if (escena)
                {
                    esce.changeall(null, null, new Vector3(0, -0.01f, 0));
                }
                else
                {
                    if (dibujo)
                    {
                        esce.changeDibujo(esce.Dactual(), null, null, new Vector3(0, -0.01f, 0));
                    }
                    else
                    {
                        esce.changeParte(esce.Dactual(), esce.dibujos.GetValueOrDefault(esce.Dactual()).Pactual(), null, null, new Vector3(0, -0.01f, 0));
                    }
                }
            }
            if (tecla == "Z")
            {
                if (dibujo)
                {
                    esce.changeActual();
                }
                else
                {
                    if (!escena)
                    {
                        esce.dibujos.GetValueOrDefault(esce.Dactual()).changeActual();
                    }
                }
            }
            return esce;
        }

        public Escenario acciones(string element, Escenario esce, Object tras, Object angle, Object scale) 
        {
            if(element == "escenario")
            {
                esce.changeall(tras, angle, scale);
            }
            else
            {
                if (esce.dibujos.ContainsKey(element))
                {
                    esce.changeDibujo(element, null, null, new Vector3(0, -0.01f, 0));
                }
                else
                {
                    String[] name = element.Split(" ");
                    if (esce.dibujos.GetValueOrDefault(name[0]).partes.ContainsKey(name[1]))
                    {
                        esce.changeParte(name[0], name[1], null, null, new Vector3(0, -0.01f, 0));
                    }
                }
            }
            return esce;
        }
    }
}

﻿using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Text;
using System.Windows.Forms;

namespace grafica.controlador
{
    class animacion
    {
        int act;
        String[] datos;
        int frames;
        bool uses = true;
        public animacion()
        {
            act = 0;
            frames = 0;
        }
        public void cargar(String data)
        {
            datos = data.Split(";");
        }
        public Escenario acciones(Escenario esce, string nombre)
        {
            JObject actual = JObject.Parse(datos[act]);
            if (uses)
            {
                if (frames < (int)actual["frames"])
                {
                    if ((bool)actual["dibujo"])
                    {
                        esce.changeDibujo(nombre, new Vector3(float.Parse(actual["tras"]["X"].ToString()), float.Parse(actual["tras"]["Y"].ToString()), float.Parse(actual["tras"]["Z"].ToString())), new Vector3(float.Parse(actual["rot"]["X"].ToString()), float.Parse(actual["rot"]["Y"].ToString()), float.Parse(actual["rot"]["Z"].ToString())), new Vector3(float.Parse(actual["sca"]["X"].ToString()), float.Parse(actual["sca"]["Y"].ToString()), float.Parse(actual["sca"]["Z"].ToString())));
                    }
                    else
                    {
                        esce.changeParte(nombre, (String)actual["nombre"], new Vector3(float.Parse(actual["tras"]["X"].ToString()), float.Parse(actual["tras"]["Y"].ToString()), float.Parse(actual["tras"]["Z"].ToString())), new Vector3(float.Parse(actual["rot"]["X"].ToString()), float.Parse(actual["rot"]["Y"].ToString()), float.Parse(actual["rot"]["Z"].ToString())), new Vector3(float.Parse(actual["sca"]["X"].ToString()), float.Parse(actual["sca"]["Y"].ToString()), float.Parse(actual["sca"]["Z"].ToString())));
                    }
                    frames++;
                }
                else
                {
                    frames = 0;
                    if (act < datos.Length - 1)
                    {
                        act++;
                    }
                    else
                    {
                        uses = false;
                    }
                }
            }
            return esce;
        }
    }
}

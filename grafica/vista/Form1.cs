﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using grafica.controlador;
using System.IO;
using System.Security;
using Newtonsoft.Json.Linq;
using grafica.Properties;
using System.Threading;

namespace grafica
{
    public partial class Form1 : Form
    {
        GLControl miglControl;
        Controller control;
        Thread tm;
        Thread ts;
        animacion Amesa;
        animacion Asilla;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            XT.Text = "0,00";
            YT.Text = "0,00";
            ZT.Text = "0,00";
            XR.Text = "0,00";
            YR.Text = "0,00";
            ZR.Text = "0,00";
            XS.Text = "0,00";
            YS.Text = "0,00";
            ZS.Text = "0,00";
            this.miglControl = new GLControl();
            control = new Controller();
            Amesa = new animacion();
            Asilla = new animacion();
            this.miglControl.BackColor = System.Drawing.Color.Purple;
            this.miglControl.Location =
                new System.Drawing.Point(0, 100);
            this.miglControl.Name = "miglControl";
            this.miglControl.Size = new System.Drawing.Size(this.Width, this.Height-100);
            this.miglControl.TabIndex = 1;
            this.miglControl.VSync = false;
            this.miglControl.Load += new System.EventHandler(this.OnLoad);
            this.miglControl.Paint += new System.Windows.Forms.PaintEventHandler(this.OnRenderFrame);
            this.miglControl.KeyDown += new KeyEventHandler(glControl_KeyDown);
            this.Controls.Add(this.miglControl);
        }
        Dibujo actualD;
        Parte actualP;
        Escenario escena = new Escenario();
        Dibujo mesa = new Dibujo();
        Dibujo silla = new Dibujo();
        private void OnLoad(object sender, EventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(0.5f, 0.0f, 0.5f, 1.0f);
            GL.Enable(EnableCap.DepthTest);

            mesa.mesa(new Vector3(0, 0, 0), 1f, 1f, 0.6f, new Vector3(-0.5f, 0.4f - 0.5f, -2.5f - 2f), new Vector3(0, 0, 0), new Vector3(1, 1, 1), new Vector2(0.0f, 0.1f), new Vector2(0.8f, 1.0f));
            silla.silla(new Vector3(0, 0, 0), 0.4f, 0.4f, 0.85f, new Vector3(-0.15f, 0.15f - 0.5f, -3f - 5f), new Vector3(0, 0, 0), new Vector3(1, 1, 1), new Vector2(0.0f, 0.1f), new Vector2(0.6f, 0.8f));
            escena.addDibujo("mesa", mesa);
            escena.addDibujo("silla", silla);
            actualD = escena.dibujos[escena.Dactual()];
            actualP = escena.dibujos[escena.Dactual()].partes[actualD.Pactual()];
            Amesa.cargar(Resources.Mesa);
            Asilla.cargar(Resources.Silla);
            tm = new Thread(animacionMesa);
            ts = new Thread(animacionSilla);
        }
        void animacionMesa()
        {
            Random a = new Random();
            int s = a.Next(0, 20);
            while (true)
            {
                escena = Amesa.acciones(escena, "mesa");
                Thread.Sleep(s);
                s = a.Next(0, 20);
            }
                
        }

        void animacionSilla()
        {
            Random a = new Random();
            int s = a.Next(0, 20);
            while (true)
            {
                escena = Asilla.acciones(escena, "silla");
                Thread.Sleep(s);
                s = a.Next(0, 20);
            }
        }
        private void OnRenderFrame(object sender, PaintEventArgs e)
        {
            this.miglControl.Size = new System.Drawing.Size(this.Width, this.Height - 100);
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
            GL.ClearColor(0.5f, 0.0f, 0.5f, 1.0f);
            GL.Viewport(0, 0, this.Width, this.Height);
            escena.render(new Vector2(this.Width, this.Height));
            miglControl.SwapBuffers();
            miglControl.Refresh();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        void glControl_KeyDown(object sender, KeyEventArgs e)
        {
            escena = control.Tecla(escena, e.KeyCode.ToString());
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            comboBox1.Items.Add("escenario");
            foreach (KeyValuePair<string, Dibujo> act in escena.dibujos)
            {
                comboBox1.Items.Add(act.Key);
                foreach (KeyValuePair<string, Parte> act2 in act.Value.partes)
                {
                    comboBox1.Items.Add(act.Key + " " + act2.Key);
                }
            }
            comboBox1.SelectedItem = "escenario";
            ts.Start();
            tm.Start();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                float tx = float.Parse(XT.Text);
                float ty = float.Parse(YT.Text);
                float tz = float.Parse(ZT.Text);
                float rx = float.Parse(XR.Text);
                float ry = float.Parse(YR.Text);
                float rz = float.Parse(ZR.Text);
                float sx = float.Parse(XS.Text);
                float sy = float.Parse(YS.Text);
                float sz = float.Parse(ZS.Text);
                escena = control.acciones(comboBox1.SelectedItem.ToString(), escena, new Vector3(tx, ty, tz), new Vector3(rx, ry, rz), new Vector3(sx, sy, sz));
                miglControl.Refresh();
            }
            catch
            {
                MessageBox.Show("Revise que los campos solo tengan valores numericos.");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
